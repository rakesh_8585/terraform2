locals {
    role = "arn:aws:iam::${var.account}:role/JenkinsRakesh"
    sg1 = "${var.sg_length == "1" ? [ var.vpc_security_group_ids1 ] : []}" #working
    sg2 = "${var.sg_length == "2" ? [ var.vpc_security_group_ids1,var.vpc_security_group_ids2 ] : []}" #working
    sg3 = "${var.sg_length == "3" ? [ var.vpc_security_group_ids1,var.vpc_security_group_ids2,var.vpc_security_group_ids3 ] : []}"
    
    #sg = "${coalesce(local.sg2,local.sg1)}" #Issue
    sg = "${coalescelist(local.sg1,local.sg2,local.sg3)}"
}

data "aws_ami" "pulse_test" {
most_recent = true
owners = ["self"]
  filter {
      name   = "name"
      values = [ var.ami ]
  }
}
provider "aws" {
  assume_role {
    role_arn = local.role
    session_name = "SESSION_NAME"
    external_id  = "EXTERNAL_ID"
  }
  region = var.region
}

resource "aws_instance" "example" {
  ami = "${data.aws_ami.pulse_test.id}"
  instance_type = var.instance_type
  key_name = var.key_name
  subnet_id = var.subnet
  vpc_security_group_ids = local.sg
  tags = {
  Name = var.Name
  Cost_Center = var.Cost_Center
  }
}
